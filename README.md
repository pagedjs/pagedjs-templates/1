---
author: Alice Ricci
title: Bischheim
project: paged.js sprint
book format: letter
book orientation: portrait
---



## Typefaces
### Sprat
https://github.com/EthanNakache/Sprat-type
SIL Open Font License

### lunchtype22
Designed by Stefan Wetterstrand
SIL Open Font License




## Supported tags
See issue
